import numpy as np

class LinearRegression:
    def __init__(self):
        self.weights = None # weights: bethas (coefficients)
    
    def fit(self, X, y):
        X_b = np.column_stack([np.ones((X.shape[0], 1)), X])           # Adding a column of ones for the intercept term
        self.weights = np.linalg.inv(X_b.T.dot(X_b)).dot(X_b.T).dot(y) # Calculating bethas (coefficients) using the normal equation

    def predict(self, X):
        X_b = np.column_stack([np.ones((X.shape[0], 1)), X])
        return X_b.dot(self.weights)
    
    def r_squared(self, X, y):
        y_pred =  self.predict(X)
        ss_reg = np.sum((y-y_pred)**2)
        ss_tot = np.sum((y-np.mean(y))**2)
        r_squared = 1 - (ss_reg/ss_tot)
        return r_squared
    
    def rmse(self, X,y):
        # y_pred = self.predict(X)
        rmse = np.sqrt(np.mean((y - self.predict(X)) ** 2))
        return rmse
