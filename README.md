#  Iman Khazrak 
## Summary
To implement a linear regression model from scratch in Python, we use the method of least squares to estimate the coefficients. Here's a breakdown of the formulas and calculations involved and in the second step we compare our with scikit-learn model:    

### Linear Regression Model       
The Linear Regression Model      
The basic linear regression model can be represented as:     
    y = β₀ + β₁X₁ + β₂X₂ + ... + βₙXₙ + ε   

Where:   
- `y` is the dependent variable.   
- `β₀` is the intercept.   
- `β₁, β₂, ..., βₙ` are the coefficients of the independent variables `X₁, X₂, ..., Xₙ`.   
- `ε` is the error term.   

#### Least Squares Estimation   
The least squares method aims to find the values of `β₀, β₁, ..., βₙ` that minimize the sum of the squared differences between the observed values and the values predicted by the model.   

#### Matrix Representation   
For computational efficiency, we use matrix operations. The model can be rewritten in matrix form as:   

    Y = X ⋅ B + ε   

    Where:   
    - `Y` is a vector of the dependent variable.   
    - `X` is a matrix of the independent variables with an added first column of 1s (for the intercept).   
    - `B` is a vector of the coefficients (including the intercept).   
    - `ε` is the vector of errors.   

#### The Normal Equation    
To solve for `B`, we use the normal equation:     

    B = (XᵀX)⁻¹XᵀY   

    Where:    
    - `Xᵀ` is the transpose of `X`.    
    - `(XᵀX)⁻¹` is the inverse of the matrix `XᵀX`.    

#### Steps for Implementation    
1. **Prepare the Data**: Organize your data into an input matrix `X` and a target vector `Y`.    
2. **Add Intercept Term**: Augment the input matrix `X` with a column of 1s at the beginning to account for the intercept.     
3. **Apply the Normal Equation**:    
   - Calculate `XᵀX`.    
   - Calculate the inverse of `XᵀX`.    
   - Multiply this inverse with `Xᵀ` and then with `Y` to get `B`.    

To evaluate the performance of a linear regression model, two common metrics are used: the coefficient of determination (R²) and the Root Mean Square Error (RMSE). Here's how you can calculate them:   

1. **Coefficient of Determination (R²)**   
   R² measures the proportion of the variance in the dependent variable that is predictable from the independent variables. It's a statistical measure between 0 and 1, where 1 indicates perfect prediction.   

            R² = 1 - (SS_res / SS_tot)   

    Where:    
    - `SS_res` is the sum of squares of residuals, ∑(y_i - ŷ_i)².   
    - `SS_tot` is the total sum of squares, ∑(y_i - ȳ)².   
    - `y_i` is the actual value.   
    - `ŷ_i` is the predicted value.   
    - `ȳ` is the mean value of `y`.    

2. **Root Mean Square Error (RMSE)**    
RMSE is a measure of how spread out these residuals are. In other words, it tells you how concentrated the data is around the line of best fit.   

    RMSE = √(∑(y_i - ŷ_i)² / n)


### Data Preprocessing   
- There is a high correlation between `dist_to_MRT` and `lat` and `longitude`, so I drop the `lat` and `longitude` columns      
![Alt text](image.png)


- `dist_to_MRT` is right skewed, so I use log transformation to deal with it.   
![Alt text](image-1.png)

- `house_age` has quadratic relationship with response.   
![Alt text](image-2.png)

- There is no influential points.   


## Result      

### Comparing mosels based on R-Squared, RMSE, and Run Time   

| Model        | R_Squared | RMSE_Train | RMSE_Test | Run_Time |
|--------------|-----------|------------|-----------|----------|
| MyModel      | 0.582481  | 8.917126   | 7.815002  | 0.002024 |
| MyModel_cv5  | 0.602687  | 8.563196   | 8.492737  | 0.021349 |
| Sklearn      | 0.582481  | 8.917126   | 7.815002  | 0.004425 |
| Sklearn_cv5  | 0.577655  | 9.053786   | 7.999592  | 0.028833 |

### Plots    

![Alt text](image-3.png)   



### Conclusion    
Our from-scratch MLR with 5-fold cross-validation really nails it because:    
- It's got a higher R-Squared, which means it's doing a great job predicting stuff.    
- The RMSE for training and testing are pretty much on the same page, showing that our model isn’t just memorizing the data – it's not overfitted.   


## Reflection     
This week, my journey in building and comparing a Linear Regression model from scratch to one using the sklearn library was enlightening and challenging. The key takeaway was the practical understanding of the theory behind Linear Regression, especially the intricate process of least squares and matrix operations for calculating coefficients. What surprised me was how close the performance of my custom model was to the sklearn implementation, as evident in the R-Squared and RMSE metrics. However, the most challenging part was dealing with data preprocessing, particularly in making decisions about dropping or transforming features like `lat`, `longitude`, and `dist_to_MRT`. These steps highlighted the importance of understanding not just the model, but also the data it's trained on, to achieve accurate predictions. This blend of theory and practical application has significantly deepened my understanding of machine learning models.   
